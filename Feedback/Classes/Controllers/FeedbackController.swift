//
//  FeedbackController.swift
//  Feedback
//
//  Created by Артем Шляхтин on 17.03.17.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import UIKit
import System
import Network
import Photos

public protocol FeedbackCustomizeDelegate: NSObjectProtocol {
    var progressViewColor: UIColor { get }
    var buttonsColor: UIColor { get }
}

public class FeedbackController: UIViewController {

    @IBOutlet weak var screenshotLabel: UILabel!
    @IBOutlet weak var screenshotImageView: UIImageView!
    @IBOutlet weak var removeScreenshotButton: UIButton!
    @IBOutlet weak var messageView: MessageView!
    @IBOutlet weak var messageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var addScreenshotButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: - Properties
    
    public weak var delegate: FeedbackCustomizeDelegate?
    public dynamic var screenshotImage: UIImage?
    fileprivate var titleView = ExtendedTitle()
    fileprivate var issueKey: String?
    
    fileprivate var bytesSent: Int64 = 0
    fileprivate var totalBytesToSend: Int64 = 0
    
    lazy var navigationProgressBar: NavigationProgressBar? = { [weak selfWeak = self] in
        let navigationProgressBar = selfWeak?.navigationController?.navigationBar as? NavigationProgressBar
        navigationProgressBar?.progressView.tintColor = selfWeak?.delegate?.progressViewColor
        return navigationProgressBar
    }()
    
    lazy var link: String = {
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "Feedback") as? [String: String],
              let site = dict["Link"]
        else { fatalError("Error loading link from plist") }
        return site
    }()
    
    lazy var projectKey: String = {
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "Feedback") as? [String: String],
              let name = dict["Project Key"]
        else { fatalError("Error loading key from plist") }
        return name
    }()
    
    lazy var issueType: String = {
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "Feedback") as? [String: String],
              let type = dict["issueType"]
        else { fatalError("Error loading user from plist") }
        return type
    }()
    
    lazy var user: String = {
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "Feedback") as? [String: String],
              let user = dict["User"]
        else { fatalError("Error loading user from plist") }
        return user
    }()
    
    // MARK: - Lifecycle
    
    public override func loadView() {
        super.loadView()
        self.addObserver(self, forKeyPath: "screenshotImage", options: .new, context: nil)
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "screenshotImage")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        messageView.delegate = self
        prepareScreenshot()
        prepareTitle()
        configureButtonColors()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Actions Outlet
    
    @IBAction func send(_ sender: UIButton) {
        if messageView.text.characters.count == 0 {
            messageView.messageTextView.becomeFirstResponder()
            return
        }
        
        messageView.messageTextView.resignFirstResponder()
        if let identifier = issueKey, let screenshot = screenshotImage {
            totalBytesToSend = screenshotBytes
            configureBeforeRequest()
            startUploadImage(forIssue: identifier, image: screenshot)
            return
        }
        
        let json = prepareJson(summary: messageView.summary, description: messageView.text)
        if let data = json.jsonData {
            let bodyBytes = Int64(data.count)
            totalBytesToSend = bodyBytes+screenshotBytes
            configureBeforeRequest()
            startCreateIssueRequest(body: data)
        }
    }
    
    @IBAction func addScreenshot(_ sender: UIButton) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            presentPhotoLibrary()
        } else if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ (granted) in
                if granted == .authorized {
                    self.presentPhotoLibrary()
                }
            })
        } else {
            let alert = UIAlertController(title: "Приложению необходим доступ к Фото", message: "Хотите ли вы изменить настройки?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                guard let url = URL(string: UIApplicationOpenSettingsURLString) else { return }
                UIApplication.shared.openURL(url)
            })
            let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            alert.addAction(ok)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteScreenshot(_ sender: UIButton) {
        screenshotImage = nil
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func closeAfterCompletedRequest() {
        perform(#selector(FeedbackController.close(_:)), with: self, afterDelay: 1.0)
    }
    
    fileprivate func presentPhotoLibrary() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    // MARK: - Animate
    
    fileprivate func animateMessageView() {
        let duration = UIApplication.shared.statusBarOrientationAnimationDuration
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - Calculate Properties 

extension FeedbackController {
    
    fileprivate var screenshotBytes: Int64 {
        get {
            var total: Int64 = 0
            guard let screenshot = screenshotImage else { return total }
            let packet = try? configureUploadImagePacket(identifier: "0", screenshot: screenshot)
            if let bytes = packet?.body?.count {
                total += Int64(bytes)
            }
            return total
        }
    }
    
}

// MARK: - Configuration

extension FeedbackController {
    
    fileprivate func configureButtonColors() {
        guard let color = delegate?.buttonsColor else { return }
        sendButton.tintColor = color
        addScreenshotButton.tintColor = color
    }
    
    fileprivate func prepareTitle() {
        configureAppearanceTitle()
        titleView.title = "Обратная связь"
        titleView.sizeToFit()
        self.navigationItem.titleView = titleView
    }
    
    fileprivate func configureAppearanceTitle() {
        if let attributes = UINavigationBar.appearance().titleTextAttributes {
            if let font = attributes[NSFontAttributeName] as? UIFont {
                titleView.titleLabel.font = font
            }
            
            if let color = attributes[NSForegroundColorAttributeName] as? UIColor {
                titleView.titleLabel.textColor = color
                titleView.subtitleLabel.textColor = color.withAlphaComponent(0.5)
            }
        }
    }
    
    fileprivate func prepareScreenshot() {
        let isExistScreenshot = (screenshotImage != nil)
        screenshotVisible(isExistScreenshot)
        screenshotImageView.image = screenshotImage
    }
    
    fileprivate func screenshotVisible(_ state: Bool) {
        screenshotLabel.isHidden = !state
        screenshotImageView.isHidden = !state
        removeScreenshotButton.isHidden = !state
        addScreenshotButton.isHidden = state
    }
    
    fileprivate func configureBeforeRequest() {
        self.bytesSent = 0
        self.navigationProgressBar?.progress = 0.0
        self.titleView.subtitle = "Отправляю..."
        self.messageView.messageTextView.isUserInteractionEnabled = false
        self.sendButton.isUserInteractionEnabled = false
    }
    
    fileprivate func configureAfterCompletedRequest() {
        self.bytesSent = 0
        self.navigationProgressBar?.progress = 0.0
        self.messageView.messageTextView.isUserInteractionEnabled = true
        self.sendButton.isUserInteractionEnabled = true
    }

}

// MARK: - Picker Controller Delegate

extension FeedbackController: UINavigationControllerDelegate {}
extension FeedbackController: UIImagePickerControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let resizeHeight = ScreenSize.Height*2
            let resizedImage = (image.size.height <= resizeHeight) ? image : image.resize(toHeight: resizeHeight)
            screenshotImage = resizedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Message View Delegate

extension FeedbackController: MessageViewDelegate {
    
    func messageViewDidUpdate(_ messageView: MessageView) {
        animateMessageView()
    }
    
}

// MARK: - Notification

extension FeedbackController {
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackController.keyboardWillShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackController.keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications(){
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShown(notification: Notification) {
        guard let info = notification.userInfo,
              let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        else { return }
        messageViewBottomConstraint.constant = keyboardSize.height
        animateMessageView()
    }
    
    func keyboardWillBeHidden(notification: Notification) {
        messageViewBottomConstraint.constant = 0
        animateMessageView()
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "screenshotImage" {
            self.screenshotImageView.image = screenshotImage
            prepareScreenshot()
        }
    }
}

// MARK: - Network Operation

extension FeedbackController {
    
    func startCreateIssueRequest(body data: Data) {
        do {
            let packet = try configureCreateIssuePacket(body: data)
            let createIssueOperation = NetworkOperation.request(packet).progress(networkOperationProgress).response(responseCreateIssue)
            createIssueOperation.start()
        } catch {
            titleView.subtitle = "Ошибка"
        }
    }
    
    func startUploadImage(forIssue key: String, image: UIImage) {
        do {
            let packet = try configureUploadImagePacket(identifier: key, screenshot: image)
            let uploadOperation = NetworkOperation.upload(packet).progress(networkOperationProgress).response(responseUploadImage)
            uploadOperation.start()
        } catch {
            titleView.subtitle = "Ошибка"
        }
    }
    
    // MARK: - Callback
    
    func networkOperationProgress(bytesSent sent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        self.bytesSent += sent
        let progress = Float(bytesSent) / Float(totalBytesToSend)
        self.navigationProgressBar?.setProgress(progress, animated: true)
    }
    
    func responseCreateIssue(result: NetworkOperationResult) {
        let jsonResult = result.data?.jsonValue as? [String: String]
        if let identifier = jsonResult?["key"], result.success {
            self.issueKey = identifier
            if let screenshot = screenshotImage {
                startUploadImage(forIssue: identifier, image: screenshot)
                return
            }
            configureAfterCompletedRequest()
            titleView.subtitle = "Готово"
            closeAfterCompletedRequest()
        } else {
            configureAfterCompletedRequest()
            titleView.subtitle = "Ошибка"
        }
    }
    
    func responseUploadImage(result: NetworkOperationResult) {
        configureAfterCompletedRequest()
        if result.success {
            titleView.subtitle = "Готово"
            closeAfterCompletedRequest()
        } else {
            titleView.subtitle = "Ошибка"
        }
    }
    
    // MARK: - Configurations

    fileprivate func prepareJson(summary: String, description: String) -> [String: Any] {
        var json = [String: Any]()
        let key = ["key": projectKey]
        let type = ["name": issueType]
        let array: [String : Any] = ["project": key, "summary": summary, "description": description, "issuetype": type]
        json["fields"] = array
        return json
    }
    
    fileprivate func configureCreateIssuePacket(body data: Data) throws -> NetworkPacket {
        guard let link = URL(string: "\(link)/rest/api/2/issue/") else { throw NetworkPacketError.invalidURL }
        var packet = NetworkPacket(url: link, body: data)
        packet.method = .POST
        packet.headers = ["Content-Type": "application/json", "Authorization": user]
        return packet
    }
    
    fileprivate func configureUploadImagePacket(identifier: String, screenshot: UIImage) throws -> NetworkPacket {
        guard let link = URL(string: "\(link)/rest/api/2/issue/\(identifier)/attachments") else { throw NetworkPacketError.invalidURL }
        guard let image = UIImagePNGRepresentation(screenshot)
        else { throw NetworkPacketError.corryptedImage }
        
        let filename = UUID()
        let file = MultiPartData(name: "\(filename).png", mimeType: "image/png", data: image)
        var packet = NetworkPacket(url: link, parts: [file])
        packet.method = .POST
        packet.headers?["Authorization"] = user
        packet.headers?["X-Atlassian-Token"] = "nocheck"
        return packet
    }
    
}

// MARK: - Gesture

extension FeedbackController {
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        messageView.messageTextView.resignFirstResponder()
    }
    
}
