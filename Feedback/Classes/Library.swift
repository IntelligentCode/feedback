//
//  Library.swift
//  Feedback
//
//  Created by Артём Шляхтин on 23/03/2017.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import Foundation

public struct FeedbackFramework {
    public static let bundleName = "ru.IntelligentCode.Feedback"
}

enum NetworkPacketError : Error {
    case invalidURL
    case corryptedImage
    case corruptedBody
}
