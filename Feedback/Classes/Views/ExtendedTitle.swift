//
//  ExtendedTitle.swift
//  Feedback
//
//  Created by Артем Шляхтин on 22.03.17.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import UIKit

// MARK: - Lifecycle

class ExtendedTitle: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titlesStackView: UIStackView!
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        performInitialization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        performInitialization()
    }
    
    private func performInitialization() {
        xibSetup()
        self.title = nil
        self.subtitle = nil
    }
    
    fileprivate func xibSetup() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        addSubview(view)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(identifier: FeedbackFramework.bundleName)
        let nib = UINib(nibName: "ExtendedTitle", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    // MARK: - Properties
    
    var subtitleView: UIView? {
        willSet (newView) {
            subtitleView?.removeFromSuperview()
            
            guard let newSubtitleView = newView else {
                subtitleLabel.isHidden = false
                return
            }
            
            subtitleLabel.isHidden = true
            titlesStackView.addArrangedSubview(newSubtitleView)
        }
    }
    
}

// MARK: - Actions

extension ExtendedTitle {
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let titleSize = titleLabel.sizeThatFits(size)
        let newWidth: CGFloat = max(titleSize.width, subtitleWidth)
        let newHeight: CGFloat = 44.0
        return CGSize(width: newWidth, height: newHeight)
    }
    
}

// MARK: - Calculated Properties

extension ExtendedTitle {
    
    var title: String? {
        get { return titleLabel.text }
        set (newTitle) {
            self.titleLabel.text = newTitle
        }
    }
    
    var subtitle: String? {
        get { return self.subtitleLabel.text }
        set (newSubtitle) {
            self.subtitleLabel.text = newSubtitle
        }
    }
    
    var subtitleWidth: CGFloat {
        get {
            let size = CGSize.zero
            let labelSize = subtitleLabel.sizeThatFits(size)
            
            guard let view = subtitleView else {
                return labelSize.width
            }
            
            let viewSize = view.sizeThatFits(size)
            return max(labelSize.width, viewSize.width)
        }
    }
    
}
