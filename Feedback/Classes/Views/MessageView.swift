//
//  MessageView.swift
//  Feedback
//
//  Created by Artem Salimyanov on 23.03.17.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import UIKit

protocol MessageViewDelegate: NSObjectProtocol {
    func messageViewDidUpdate(_ messageView: MessageView)
}

class MessageView: UIView {

    @IBOutlet weak var borederView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    weak var delegate: MessageViewDelegate?
    
    // MARK: - Constants
    
    fileprivate let defaultHeight: CGFloat = 41.0
    fileprivate let maxHeight: CGFloat = 126.0
    fileprivate let offset: CGFloat = 4.0
    
    // MARK: - Computed Vars
    
    lazy var heightLayoutConstraint: NSLayoutConstraint? = {
        let height = self.constraints.first{ $0.identifier == "HeightMessageView" }
        return height
    }()
    
    var text: String {
        get { return messageTextView.text }
    }
    
    var summary: String {
        get {
            let worlds = messageTextView.text.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            if worlds.count > 4 { return "\(worlds[0]) \(worlds[1]) \(worlds[2]) \(worlds[3]) \(worlds[4])" }
            else { return self.text }
        }
    }
    
    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        initialConfiguration()
    }
    
    // MARK: - Configuration
    
    fileprivate func initialConfiguration() {
        messageTextView.delegate = self
        messageTextView.isScrollEnabled = false
        placeholderLabel.isHidden = false
    }
    
    fileprivate func checkPlaceholderVisible() {
        placeholderLabel.isHidden = (messageTextView.text.isEmpty) ? false : true
    }
    
    fileprivate func updateHeight() {
        let size = messageTextView.sizeThatFits(messageTextView.bounds.size)
        var height = size.height
        if height < defaultHeight { height = defaultHeight }
        if height >= defaultHeight {
            if height >= maxHeight {
                messageTextView.isScrollEnabled = true
                return
            } else {
                messageTextView.isScrollEnabled = false
                heightLayoutConstraint!.constant = height + offset
                delegate?.messageViewDidUpdate(self)
            }
        }
    }
}

// MARK: - Text View Delegate

extension MessageView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        checkPlaceholderVisible()
        updateHeight()
    }
}
