//
//  NavigationProgressBar.swift
//  Feedback
//
//  Created by Artem Salimyanov on 24.03.17.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import UIKit

protocol NavigationProgressBarProtocol: NSObjectProtocol {
    var progress: Float { set get }
    func setProgress(_ progress: Float, animated: Bool)
}

class NavigationProgressBar: UINavigationBar {
    
    let progressView = UIProgressView(progressViewStyle: .bar)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configProgressView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configProgressView()
    }
    
    fileprivate func configProgressView() {
        self.addSubview(progressView)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: progressView, attribute: .bottom, multiplier: 1, constant: 1.0)
        let leftConstraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: progressView, attribute: .leading, multiplier: 1, constant: 0.0)
        let rightConstraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: progressView, attribute: .trailing, multiplier: 1, constant: 0.0)
        let heightConstraint = NSLayoutConstraint(item: progressView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1.0)
        
        progressView.addConstraint(heightConstraint)
        self.addConstraints([bottomConstraint, leftConstraint, rightConstraint])
    }

}

extension NavigationProgressBar: NavigationProgressBarProtocol {
    
    var progress: Float {
        get { return progressView.progress }
        set (newValue) {
            progressView.progress = newValue
        }
    }
    
    func setProgress(_ progress: Float, animated: Bool) {
        progressView.setProgress(progress, animated: animated)
    }
    
}
